from django.shortcuts import render, get_object_or_404, redirect
from todos.models import TodoList, TodoItem
from todos.forms import TodoListForm


# Create your views here.
def todo_list(request):
    todolist = TodoList.objects.all()
    context = {
        "todo_list": todolist,
    }
    return render(request, "todos/list.html", context)

def todo_list_detail(request, id):
    show_list_item = get_object_or_404(TodoList, id=id)
    context = {
        "todo_list_detail": show_list_item,
    }
    return render(request, "todos/details.html", context)


def todo_list_create(request):
    if request.method == "POST":
        form = TodoListForm(request.POST)
        if form.is_valid():
            todo_list = form.save()
            return redirect("todo_list_detail", id=todo_list.id)
    else:
        form = TodoListForm()
    context = {
        "form": form,
    }
    return render(request, "todos/create.html", context)


def todo_list_update(request, id): 
    todo_list = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        form = TodoListForm(request.POST, instance=todo_list)
        if form.is_valid():
            form.save()
            return redirect("todo_list_detail", id=id)
    else:
        form = TodoListForm(instance=todo_list)

    context = {
        "form": form,
        "todo_list_object": todo_list,
    }
    return render(request, "todos/edit.html", context)


def delete_list(request, id):
    todo_list = TodoList.objects.get(id=id)
    if request.method == "POST":
        todo_list.delete()
        return redirect("todo_list")
    return render(request, "todos/delete.html")

def todo_item_create(request, id):
    if request.method == "POST":
        form = TodoItem(request.POST,instance=todo_list_detail)
        if form.is_valid():
            form.save()
            return redirect("todo_list_detail", id=id)
    else:
        form = TodoItem(instance=todo_list_detail, id=id)

    context = {
        "form": form,
        "todo_item_object": todo_list_detail
    }
    return render(request, "todos/create.html", context)